from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Ringgi Cahyo Dwiputra' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,2,25) #TODO Implement this, format (Year, Month, Date)
npm = 1706025005 # TODO Implement this
tempat_kuliah = "Universitas Indonesia"
hobi = "to code a program"
desc = "I'm a funny man"

#Friend 1 Right
name1 = "Muhammad Feril Bagus Prakasa"
npm1 = 1706075054
age1 = date(1999,7,8)
tempat_kuliah1 = "Universitas Indonesia"
hobi1 = "gaming"
desc1 = "He likes to code also"

#Friend 2 Left
name2 = "Muhammad Ardivan Satrio Nugroho"
npm2 = 1706025371
age2 = date(1999,8,14)
tempat_kuliah2 = "Universitas Indonesia"
hobi2 = "playing guitar"
desc2 = "He is a 'Pria Punya Selera' man"

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, "tempat_kuliah" : tempat_kuliah, "hobi" : hobi, "desc" : desc,
                "name1": name1, "age1": calculate_age(age1.year), "npm1": npm1, "tempat_kuliah1": tempat_kuliah, "hobi1": hobi1, "desc1": desc1,
                "name2": name2, "age2": calculate_age(age2.year), "npm2": npm2, "tempat_kuliah2": tempat_kuliah, "hobi2": hobi2, "desc2": desc2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
